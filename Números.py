def Mostrarnúmero(num):
    if num == 0:
        print("Se ha cerrado el programa")
    elif num <=5:
        print("Número pequeño")
    elif num <=10:
        print("Número mediano")
    else:
        print("Número grande")

while True:
    try:
        var_num = int(input("Introduce un número: "))
        Mostrarnúmero(var_num)
        if var_num == 0:
            break
    except ValueError:
        print("Por favor, introduzca un número válido.")
